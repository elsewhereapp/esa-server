########Maven Image########
FROM maven:3.8.4-jdk-11
WORKDIR /root

# Install Maven dependencies

# Install the ESA score package
RUN git clone https://gitlab.com/elsewhereapp/esa-score esa-score
RUN mvn -f esa-score/pom.xml clean install -Dmaven.test.skip

# Install the ESA core package
RUN git clone https://gitlab.com/elsewhereapp/esa-core esa-core
RUN mvn -f esa-core/pom.xml clean install -Dmaven.test.skip

# Install the ESA config package
RUN git clone https://gitlab.com/elsewhereapp/esa-config esa-config
RUN mvn -f esa-config/pom.xml clean install -Dmaven.test.skip

#copy the code
COPY src /root/src
COPY esa.sh /root
COPY pom.xml /root
COPY README.md /root
COPY LICENSE /root
COPY esa-api.yaml /root
COPY document-scores.esa /root
COPY term-index.esa /root
COPY esa.properties /root

# Just compile the dang thing here
RUN mvn package
RUN cp /root/target/*.jar /root && rm -Rf /root/target && mkdir /root/target && mv /root/*.jar /root/target
RUN rm -Rf /root/esa-core
RUN rm -Rf /root/esa-score
RUN chmod +x /root/esa.sh

#run the app
ENTRYPOINT exec ./esa.sh
