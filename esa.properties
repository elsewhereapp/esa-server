###################################################
########## Properties specific to indexing ########
###################################################

### Wikipedia source and output
indexing.sourceDirectory = /data/esa/wiki
indexing.outputDirectory = /data/esa/index
indexing.source = simplewiki-20220320

### Indexing stages (options: preprocess, id-titles, count, repeat, rare-terms, index, stats, debug)
#indexing.stage.0 = preprocess
#indexing.stage.1 = id-titles
#indexing.stage.2 = count
#indexing.stage.3 = rare-terms
indexing.stage.4 = index


### Stage-specific Options

### Preprocess
# see articleFilter.properties for article title exclusions

## Repeat

# How many times to duplicate links
indexing.repeat.links = 0

# How many times to repeat the titles
indexing.repeat.titles = 0

## Rare Words

# Rare world threshold (how many articles a term has to occur in before it is kept)
indexing.rare.threshold = 3

## Indexing
# see analyzer.properties, articleFilter.properties for the settings to use

### Stats


### Threading Information
indexing.threads.count = 20
indexing.threads.batchSize = 1000

###################################################
######### Properties for article filtering ########
###################################################


### Article Filtering (used by various stages) ###

## Minimum incoming links
article.minInLinks =

## Minimum outgoing links
article.minOutLinks =

## Minimum terms in article
article.minTerms =

## Maximum terms in article
article.maxTerms =

## Article title exclusions
article.titleExclusion.0.regexp = ^(?!category:)[^: ]+:[^ ].+$
article.titleExclusion.1.regexp = ^(ad )?[0-9]+s?( bc)?$
article.titleExclusion.2.regexp = ^(january|february|march|april|may|june|july|august|september|october|november|december) [0-9]{1,2}( [0-9]{4})?$
article.titleExclusion.3.regexp = ^category talk:.+$


###################################################
######## Scoring options for TF-IDF / BM25 ########
###################################################

### The scoring options to configure TF-IDF / BM25 and similar ###

## The scoring mode which can be one of the following:
# A specific implementation of BM25:
# 1. BM25
# 2. BM25+
# 3. BM11
# 4. BM15
#
# Or it can be sequence of three letters denoting different TF-IDF algorithms per
# https://nlp.stanford.edu/IR-book/html/htmledition/document-and-query-weighting-schemes-1.html
# Check the code to see which letter codes are available for each section.
#
# The three letter code can also be prefixed with "b:" to make it BM25
scoring.mode = nbn

# The "b" parameter of BM25 (traditionally 0.75)
# This can be 1 for BM11 or 0 ro BM15
scoring.bm25.b = 0.75

# The "k" parameter of BM25 (traditionally 1.2 or 2.0)
scoring.bm25.k = 1.2

# The optional delta which turns BM25 into BM25+ (traditionally set to 1.0)
scoring.bm25.delta = 1.0



###################################################
####### Collection options (for read/write) #######
###################################################

## Collection Source

# The file with an index of terms to their document scores (also has IDF info)
collection.termIndexSource = ./term-index.esa

# The file containing document scores for each term
collection.documentScoreSource = ./document-scores.esa

## Collection Access and Caching

# Specifies how to access the document scores (disk, memory)
# Disk is pretty fast, and on a good NVME SSD is basically as fast as RAM.
# The document score files tend to be quite large (~3GB)
# so loading to RAM may not always be the best option
collection.access = memory

# Caching mechanism (none, top-hits)
# "top-hits" always saves the most accessed 2,048 terms which may have slight performance benefits
# most of the time, you probably won't need it as the system is so fast to begin with
collection.cache = none

# Whether to log collection and query information to a file
collection.log.enabled = false
collection.log.output = esa-collection.log

# The id-title mappings for displaying document titles as text
collection.titleSource =

###################################################
######### Vectorization and tuning options ########
###################################################

## Vectorization Options
## These are currently not used for indexing, but at some point, the system would use them to prebuild indexes with vectorization options to make them smaller and more performant

# The size of the sliding window
vector.windowSize = 0

# The maximum allowed percentage drop from the first item in the window to the last
vector.windowDrop = 0

# The maximum number of entries in a vector
vector.limit = 4000

# The additional weight to apply to concepts appearing more than once in a vector
vector.conceptMultiplier = 1.28

###################################################
########## Analyzer filters (from Lucene) #########
###################################################

# Classic (removes apostrophes and periods)
analyzer.classic.enabled = true

# Singular (removes all plurals)
analyzer.singular.enabled = true

# Ascii (converts unicode to ascii where possible)
analyzer.ascii.enabled = true

# Lowercase (converts all text to lowercase)
analyzer.lowercase.enabled = true

# Stemmer (stems all words)
analyzer.stemmer.enabled = true
# Stemmer depth (how many times to apply the stemmer per term)
analyzer.stemmer.depth = 3


## Dictionary Options

## Go Words (all other terms are excluded)

# A file containing a word on each line
analyzer.go.0.source =

## Stop Words (these terms are excluded)

# A file containing a word on each line
analyzer.stop.0.source = ./src/main/resources/en-stopwords.txt

## Preprocessors (these transform at the document level)

# The enabled preprocessors (standard: basic stripping of symbols and punctuation, wiki: converts wikipedia articles to plain text)
analyzer.preprocessor.0 = standard

## Term Filtering

# Minimum term length
analyzer.term.minLength = 3
# Maximum term length
analyzer.term.maxLength = 32


###################################################
############## Vector Tuning Options ##############
###################################################


# The action to perform (tune, print-vector, score-compare)
tuning.action = tune

### Action-specific options

## Vector printing

# Prints the vector for the specified text
tuning.print-vector.text = cat

## Score comparisons between Spearman sets
## This helps determine how one Spearman list compares to another

# Spearman CSV files
tuning.score-compare.spearmanCsv1 =
tuning.score-compare.spearmanCsv2 =

# Spearman term to compare
# If omitted, entire sets are compared
tuning.score-compare.term =


### Options for tuning vector settings to find the best Spearman scores for word similarity
###  and Pearson scores for document similarity

## The "window" is a sliding window that moves across document scores for a term
## The scores are sorted from highest to lowest.
##

# The initial size of the window
tuning.tune.windowStart = 5

# The ending size of the window
tuning.tune.windowEnd = 15

# The step to use for each iteration of the window
tuning.tune.windowStep = 1

## Window drop is the percentage to use when determining if the first score in the window
## and the last score in the window are too similar to keep going
## This roughly translates to being in the "tail" of the scores
## At this point we truncate the window and only keep the scores seen so far

# The starting drop off
tuning.tune.dropOffStart = 0.0000001

# The ending drop off
tuning.tune.dropOffEnd = 0.000001

# The drop off step
tuning.tune.dropOffStep = 0.0000001

## Vector limits are the simplest form of tuning and only keep the highest N document-scores per term
## The developer who wrote these docs doesn't think that you can tune with both window options and vector limit options--pick one or the other.

# The vector limit start
tuning.tune.vectorLimitStart = 500

# The vector limit end
tuning.tune.vectorLimitEnd = 700

# The vector limit step
tuning.tune.vectorLimitStep = 1


# The type of tuning can either be spearman or pearson
tuning.tune.type = spearman

tuning.tune.output = ./spearman-scores.csv
