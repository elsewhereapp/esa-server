java -Djdbc.drivers=org.postgresql.Driver -DtotalEntitySizeLimit=2147480000 -Djdk.xml.totalEntitySizeLimit=2147480000 -Xmx32g -jar target/api-*.jar "$@"
