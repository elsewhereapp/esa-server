package com.elsewhere.esa.api;

import com.dreamcloud.esa_config.cli.AnalyzerOptionsReader;
import com.dreamcloud.esa_config.cli.CollectionOptionsReader;
import com.dreamcloud.esa_config.cli.TfIdfOptionsReader;
import com.dreamcloud.esa_config.cli.VectorizationOptionsReader;
import com.dreamcloud.esa_core.analyzer.AnalyzerOptions;
import com.dreamcloud.esa_core.analyzer.EsaAnalyzer;
import com.dreamcloud.esa_core.analyzer.TokenizerFactory;
import com.dreamcloud.esa_core.similarity.DocumentSimilarity;
import com.dreamcloud.esa_core.vectorizer.VectorBuilder;
import com.dreamcloud.esa_core.vectorizer.VectorizationOptions;
import com.dreamcloud.esa_core.vectorizer.Vectorizer;
import com.dreamcloud.esa_score.analysis.TfIdfAnalyzer;
import com.dreamcloud.esa_score.analysis.TfIdfOptions;
import com.dreamcloud.esa_score.analysis.TfIdfStrategyFactory;
import com.dreamcloud.esa_score.analysis.strategy.TfIdfStrategy;
import com.dreamcloud.esa_score.fs.CollectionOptions;
import com.dreamcloud.esa_score.score.DocumentNameResolver;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Properties;

@SpringBootApplication
public class Application {
    public static void main(String[] args) throws IOException {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public DocumentSimilarity textVectorizer() throws IOException {
        //Set up the ESA properties
        String propertiesPath =  "./esa.properties";
        Properties properties = new Properties();
        properties.load(new FileInputStream(propertiesPath));

        System.out.println("Properties");
        System.out.println("----------------------------------------");
        properties.stringPropertyNames().stream().sorted().forEach((String property) -> {
            System.out.println(property + "\t\t=  " + properties.getProperty(property));
        });
        System.out.println("----------------------------------------\n");

        AnalyzerOptionsReader analyzerOptionsReader = new AnalyzerOptionsReader();
        VectorizationOptionsReader vectorizationOptionsReader = new VectorizationOptionsReader();
        TfIdfOptionsReader tfIdfOptionsReader = new TfIdfOptionsReader();
        CollectionOptionsReader collectionOptionsReader = new CollectionOptionsReader();

        VectorizationOptions vectorOptions = vectorizationOptionsReader.getOptions(properties);
        AnalyzerOptions analyzerOptions = analyzerOptionsReader.getOptions(properties);
        TfIdfOptions tfIdfOptions = tfIdfOptionsReader.getOptions(properties);
        CollectionOptions collectionOptions = collectionOptionsReader.getOptions(properties);

        analyzerOptions.setTokenizerFactory(new TokenizerFactory() {
            public Tokenizer getTokenizer() {
                return new StandardTokenizer();
            }
        });

        TfIdfStrategyFactory tfIdfFactory = new TfIdfStrategyFactory();
        TfIdfStrategy tfIdfStrategy = tfIdfFactory.getStrategy(tfIdfOptions);
        TfIdfAnalyzer tfIdfAnalyzer = new TfIdfAnalyzer(tfIdfStrategy, new EsaAnalyzer(analyzerOptions), collectionOptions.getTermIndex());

        VectorBuilder vectorBuilder = new VectorBuilder(collectionOptions.getScoreReader(), tfIdfAnalyzer, analyzerOptions.getPreprocessor(), vectorOptions);
        return new DocumentSimilarity(new Vectorizer(vectorBuilder));
    }

    @Bean
    public DocumentNameResolver documentNameResolver() throws IOException {
        //Set up the ESA properties
        String propertiesPath =  "./esa.properties";
        Properties properties = new Properties();
        properties.load(new FileInputStream(propertiesPath));

        CollectionOptions collectionOptions = new CollectionOptionsReader().getOptions(properties);
        return collectionOptions.getDocumentNameResolver();
    }

    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
        return args -> {

            System.out.println("Let's inspect the beans provided by Spring Boot:");

            String[] beanNames = ctx.getBeanDefinitionNames();
            Arrays.sort(beanNames);
            for (String beanName : beanNames) {
                System.out.println(beanName);
            }

        };
    }
}
