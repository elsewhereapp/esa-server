package com.elsewhere.esa.api;

public class DocumentSimilarityRequestBody {
    public String documentText1;
    public String documentText2;
}
