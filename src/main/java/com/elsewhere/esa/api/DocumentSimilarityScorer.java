package com.elsewhere.esa.api;

import com.dreamcloud.esa_core.similarity.DocumentSimilarity;
import com.dreamcloud.esa_core.similarity.SimilarityInfo;
import com.dreamcloud.esa_core.vectorizer.TextVectorizer;
import com.dreamcloud.esa_score.score.DocumentNameResolver;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;

public class DocumentSimilarityScorer {
    @Autowired
    private DocumentNameResolver nameResolver;
    private final TextVectorizer vectorizer;

    //todo: fix API stuff so it's all nice and separate from command line
    public DocumentSimilarityScorer(TextVectorizer vectorizer) {
        this.vectorizer = vectorizer;
    }

    public DocumentScore score(DocumentSimilarityRequestBody request) throws Exception {
        DocumentSimilarity similarity = new DocumentSimilarity(vectorizer);
        try {
            SimilarityInfo similarityInfo = similarity.score(request.documentText1, request.documentText2, true);
            ArrayList<String> parsedTopConcepts = new ArrayList<>();
            for(String topConcept: similarityInfo.getTopConcepts()) {
                parsedTopConcepts.add(nameResolver.getTitle(Integer.parseInt(topConcept)));
            }
            return new DocumentScore("success", similarityInfo.getScore(), parsedTopConcepts.toArray(new String[0]));
        } catch (Exception e) {
            e.printStackTrace();
            return new DocumentScore("failure", e.getMessage());
        }
    }
}
