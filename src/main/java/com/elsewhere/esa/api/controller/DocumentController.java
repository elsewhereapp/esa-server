package com.elsewhere.esa.api.controller;

import com.elsewhere.esa.api.Document;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestController
@CrossOrigin(origins="*")
public class DocumentController {
    @GetMapping("/documents")
    public List<Document> getDocuments() {
        List<Document> docs = new ArrayList<>();
        Document doc1 = new Document();
        doc1.id = "1";
        doc1.title = "Cat";
        doc1.text = "Cats are cute, furry mammals.";
        doc1.inLinks = 1000;
        doc1.outLinks = 50;
        doc1.terms = 1500;
        docs.add(doc1);

        Document doc2 = new Document();
        doc2.id = "2";
        doc2.title = "Dog";
        doc2.text = "Cats are furry mammals.";
        doc2.inLinks = 900;
        doc2.outLinks = 40;
        doc2.terms = 1400;
        docs.add(doc2);

        return docs;
    }

    @GetMapping("/documents/{id}")
    public Document getDocument(@PathVariable String id) {
        List<Document> docs = this.getDocuments();
        if ("1".equals(id)) {
            return docs.get(0);
        } else if("2".equals(id)) {
            return docs.get(1);
        } else {
            throw new ResponseStatusException(NOT_FOUND, "Unable to find resource");
        }
    }
}
