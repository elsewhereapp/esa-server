package com.elsewhere.esa.api.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IndexController {
    @GetMapping("/")
    public String index() {
        return "<p><i>Welcome to Elsewhere's explicit semantic analysis (ESA) REST API.</i></p><p><small>You can find the documentation here: <a href='/v3/api-docs.yaml'>/v3/api-docs.yaml</a></small></p>";
    }
}
