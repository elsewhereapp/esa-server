package com.elsewhere.esa.api.controller;

import com.dreamcloud.esa_core.similarity.DocumentSimilarity;
import com.dreamcloud.esa_core.similarity.SimilarityInfo;
import com.elsewhere.esa.api.DocumentSimilarityRequestBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@CrossOrigin(origins = "*")
public class SimilarityController {
    @Autowired
    private DocumentSimilarity documentSimilarity;

    @PostMapping("/similarity")
    public SimilarityInfo getSimilarity(@RequestBody DocumentSimilarityRequestBody requestBody) throws Exception {
        return documentSimilarity.score(requestBody.documentText1, requestBody.documentText2, true);
    }
}
